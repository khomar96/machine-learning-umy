# Data Preprocessing Template

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Position_Salaries.csv')
X = dataset.iloc[:, 1:2].values#1ère colonne, X will be considered as a matric
y = dataset.iloc[:, 2].values#2ème colonne



#fitting linear Regression to the dataset
from sklearn.linear_model import LinearRegression
lin_reg=LinearRegression()
lin_reg.fit(X, y)



X_grid= np.arange(min(X),max(X),0.1)
X_grid=X_grid.reshape((len(X_grid),1))#to predict the whole range 


#fitting Polynomial Regression to the dataset
from sklearn.preprocessing import PolynomialFeatures
poly_reg=PolynomialFeatures(degree=4) #will contain  a matric x1, x1^2,...x^n
X_poly=poly_reg.fit_transform(X)
lin_reg_2=LinearRegression()
lin_reg_2.fit(X_poly,y)
print(X)
print(X_poly)
#visualising the Linear Regression results
plt.scatter(X,y,color='red')#plotting the real results we have 
plt.plot(X,lin_reg.predict(X))#plotting the results predicted
plt.title('Truth or Bluff')
plt.xlabel('Position level')
plt.ylabel('Salary')
#Visualising the Polynomial Regression results
plt.scatter(X,y,color='red')#plotting the real results we have 
plt.plot(X_grid,lin_reg_2.predict(poly_reg.fit_transform(X_grid)))#plotting the results predicted
plt.title('Truth or Bluff')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()



#predicting a new result with linear  Regression 
print(lin_reg.predict([[6.5]]))#predict the salary of a 6.5 level


#fitting Polynomial Regression to the dataset


# Splitting the dataset into the Training set and Test set
#from sklearn.model_selection import train_test_split
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train.reshape(-1,1))"""