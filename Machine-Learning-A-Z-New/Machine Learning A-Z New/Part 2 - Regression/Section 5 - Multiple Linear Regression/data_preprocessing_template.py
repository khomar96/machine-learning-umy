# Data Preprocessing Template

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('50_Startups.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 4].values
print(X)
#######
print(y)
#LabelEncoder to encode text variables, OneHotEncoder to manage the dummy variables
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
labelencoder_X=LabelEncoder()
X[:,3]=labelencoder_X.fit_transform(X[:,3])
onehotencoder=OneHotEncoder(categorical_features=[3])
X=onehotencoder.fit_transform(X).toarray()
print(X)




#Avoiding the dummy variable Trap

X=X[:,1:]
# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

#fitting Multiple Linear Regression to the Training set

from sklearn.linear_model import LinearRegression
regressor=LinearRegression()
regressor.fit(X_train,y_train)

#fitting Multiple Linear Regression to the Test set
y_pred=regressor.predict(X_test)
print(y_pred)


#to find a team of independent variables that have an impact on the dependent variable
#Here we gonna prepare for backward elimination

import statsmodels.formula.api as sm
#y=b0+b1*x1+...bn*xn
#because of b0 we add a 1 b0=b0*1
#we're gonna add a colum of 1
X=np.append(arr=np.ones((50,1).astype(int),values=X,axis=1))

X_opt=X[:,[0,1,2,3,4,5]]#Matrix containing all the independant variables


#Step 1 :Select a significance level to stay in a model (e.g SL=0.05)
#SL=0.05

#Step 2:fit the full model with all possible predictors
regressor_OLS=sm.OLS(endog=y,exog=X_opt).fit()#two parameters:1-enddog:dependant varianble 2-exog=X (matric of features)

#Step 3: to spot the PL we're gonna use this tool
#the lower the P value is , the more significance the independance variable are gonna be in respect with the dependant variable

regressor_OLS.summary()

#Next video
X_opt=X[:,[0,1,3,4,5]]#We remove the 2 which is related with the highest P value
regressor_OLS=sm.OLS(endog=y,exog=X_opt).fit()#two parameters:1-enddog:dependant varianble 2-exog=X (matric of features)
regressor_OLS.summary()

#We're gonna remove x1 with the highest Pvalue
X_opt=X[:,[0,3,4,5]]#We remove the 2 which is related with the highest P value
regressor_OLS=sm.OLS(endog=y,exog=X_opt).fit()#two parameters:1-enddog:dependant varianble 2-exog=X (matric of features)
regressor_OLS.summary()
#We're gona remove index 4
X_opt=X[:,[0,3,5]]#We remove the 2 which is related with the highest P value
regressor_OLS=sm.OLS(endog=y,exog=X_opt).fit()#two parameters:1-enddog:dependant varianble 2-exog=X (matric of features)
regressor_OLS.summary()

#We're gona remove the index 5
X_opt=X[:,[0,3]]#We remove the 2 which is related with the highest P value
regressor_OLS=sm.OLS(endog=y,exog=X_opt).fit()#two parameters:1-enddog:dependant varianble 2-exog=X (matric of features)
regressor_OLS.summary()



"""automatic backward elimination with p-only
import statsmodels.formula.api as sm
def backwardElimination(x, sl):
    numVars = len(x[0])
    for i in range(0, numVars):
        regressor_OLS = sm.OLS(y, x).fit()
        maxVar = max(regressor_OLS.pvalues).astype(float)
        if maxVar > sl:
            for j in range(0, numVars - i):
                if (regressor_OLS.pvalues[j].astype(float) == maxVar):
                    x = np.delete(x, j, 1)
    regressor_OLS.summary()
    return x
 
SL = 0.05
X_opt = X[:, [0, 1, 2, 3, 4, 5]]
X_Modeled = backwardElimination(X_opt, SL)

"""


# Feature Scaling
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train.reshape(-1,1))"""