# Data Preprocessing Template

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Salary_Data.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 1].values

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1/3, random_state = 0)
print(X_train)


# Feature Scaling
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train.reshape(-1,1))"""

from sklearn.linear_model import LinearRegression

#to create object from this LinearRegression class
regressor= LinearRegression()
#we need to  fit the training set
regressor.fit(X_train,y_train)
#create vector of the prediction of the test set salaries
y_pred=regressor.predict(X_test)#makes sens
#print(y_pred)
#print("*****")
#print(y_test)
#y_test are the real values
#y_pred are the predict salaries

#visualising the Training set results
'''
plt.scatter(X_train,y_train, color='red')

plt.plot(X_train,regressor.predict(X_train),color='blue')

plt.title("Salary vs Experience (Training set)")
plt.xlabel("Years of experience")
plt.ylabel("Salary")
plt.show()
'''
#visualising the test set results plt.scatter(X_train,y_train, color='red')
plt.scatter(X_test,y_test, color='red')

plt.plot(X_train,regressor.predict(X_train),color='blue')

plt.title("Salary vs Experience (Test set)")
plt.xlabel("Years of experience")
plt.ylabel("Salary")
plt.show()